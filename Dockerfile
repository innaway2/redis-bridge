FROM 524951117063.dkr.ecr.ap-southeast-1.amazonaws.com/node-18 AS base

WORKDIR /app

FROM base AS builder

COPY . /app
RUN yarn
RUN yarn build

# ---------- Release ----------
FROM base AS release

ENV NODE_ENV=production
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist
USER node

ENV VIRTUAL_HOST=redis-bridge.innaway.co
ENV LETSENCRYPT_HOST=redis-bridge.innaway.co
ENV VIRTUAL_PORT=3000

ENV REDIS_PORT=6379
ENV REDIS_HOST=52.220.22.53
ENV REDIS_PASSWORD=Innaway@123

CMD ["node", "dist/index.js"]