#!/bin/bash
ENV_PATH=".env"
echo Generate env file ...

echo "REDIS_PORT=$REDIS_PORT" >> $ENV_PATH
echo "REDIS_HOST=$REDIS_HOST" >> $ENV_PATH
echo "REDIS_PASSWORD=$REDIS_PASSWORD" >> $ENV_PATH
echo "NODE_ENV=$NODE_ENV" >> $ENV_PATH

echo "$(cat .env)"