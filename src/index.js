import express from 'express';
import cors from 'cors';
import { errorHandler, errorConverter } from '@Middlewares/error';
import routes from './routes';
import RedisBridge from '@Utils/redisBridge';
require('dotenv').config();

(async () => {
  const app = express();
  app.use(express.json());
  app.use(cors());

  app.use(errorConverter);
  app.use(errorHandler);
  app.use('/', routes);
  console.log(process.env);
  const redisBridge = new RedisBridge(app);
  const server = await redisBridge.execute();

  app.get('/', (req, res) => {
    res.json({ message: 'Welcome to redis bridge production for message' });
  });

  server.listen(3000, () => {
    console.log('Server is running on port 3000.');
  });
})();
