import catchAsync from '@Utils/catchAsync';
import { createClient } from 'redis';
import 'dotenv/config';

export const testMessage = catchAsync(async (req, res) => {
  const { channel, value } = req.query;
  const client = createClient({
    url: `redis://:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
  });

  client.on('error', (err) => console.log('Redis Client Error', err));
  await client.connect();
  await client.set(channel, value);
  await client.publish(channel, value);
  const valueRedis = await client.get(channel);

  res.send(valueRedis);
});
