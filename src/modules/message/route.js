import express from 'express';
import { testMessage } from './controller';

const router = express.Router();

router.route('/').get(testMessage);

export default router;
