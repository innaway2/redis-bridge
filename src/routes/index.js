import express from 'express';
import messageRouter from '../modules/message/route';

const router = express.Router();

router.get('/status', (req, res) => res.send('OK'));

router.use('/message', messageRouter);

export default router;