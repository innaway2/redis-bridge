import { Server } from 'socket.io';
import { createClient } from 'redis';
import { createAdapter } from '@socket.io/redis-adapter';
import http from 'http';
import cors from 'cors';
require('dotenv').config();

export default class RedisBridge {
  constructor(app) {
    app.use(cors());
    this.server = http.createServer(app);
  }

  async execute() {
    const io = new Server(this.server, {
      cors: {
        origin: true,
        credentials: true,
      },
      allowEIO3: true,
      transports: ['websocket', 'polling', 'flashsocket'],
    });
    const pubClient = createClient({
      url: `redis://:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
    });
    const subClient = pubClient.duplicate();
    pubClient.on('error', (err) => console.log('Redis Client Error', err));

    Promise.all([pubClient.connect(), subClient.connect()]).then(() => {
      io.adapter(createAdapter(pubClient, subClient));
      io.listen(3001);
    });

    io.on('connection', (socket) => {
      subClient.pSubscribe('*', (message, channel) => {
        console.log('message', message);
        console.log('channel', channel);
        socket.emit(channel, message);
      });
    });

    io.on('disconnect', function () {
      subClient.quit();
    });

    return this.server;
  }
}
